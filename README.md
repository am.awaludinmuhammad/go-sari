<!-- PROJECT LOGO -->
<br />
<div align="center">
     <!--<a href="https://github.com/github_username/repo_name">
      <img src="images/logo.png" alt="Logo" width="80" height="80"> -->
  </a>

<h1 align="center">Go Sari</h1>

</div>

<!-- ABOUT THE PROJECT -->
## Tentang Proyek

Go Sari merupakan sebuah aplikasi manajemen user berbasis website yang dibuat menggunakan Laravel. Proyek ini dibuat berdasarkan studi kasus Unit Go Sari dari BUMDes Guwo Sari Maju, Kab. Bantul, Yogyakarta.

### Teknologi yang digunakan

* [Laravel](https://laravel.com)
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)


<!-- GETTING STARTED -->

### Persiapan

Hal yang perlu anda persiapkan untuk menjalankan aplikasi ini:

* PHP versi 7.2.5 | 8.0
* Composer

### Instalasi
1. Clone repository
	a. SSH
   ```sh
   git clone git@gitlab.com:am.awaludinmuhammad/go-sari.git
   ```
   b. HTTPS
      ```sh
   git clone https://gitlab.com/am.awaludinmuhammad/go-sari.git
   ```
2. Install Composer Package
   ```sh
   composer install
   ```
3. Setup file .env
	* Anda bisa mengkopi atau mengganti nama file .env.example ke dalam file .env
	* Atur konfigurasi database
	* Atur konfigurasi MAIL 
   ```
   MAIL_DRIVER=smtp
   MAIL_HOST=smtp.gmail.com
   MAIL_PORT=465
   MAIL_USERNAME="{email anda}"
   MAIL_PASSWORD="{password email anda}"
   MAIL_ENCRYPTION=ssl
   MAIL_FROM_ADDRESS="{email anda}"
   MAIL_FROM_NAME="${APP_NAME}"
   ```
4. Generate Key
   ```sh
   php artisan key:generate
   ```
5. Migrate tabel
   ```sh
   php artisan migrate
   ```
6. Sampai pada tahap ini, aplikasi sudah dapat dijalankan.
   ```sh
   php artisan serve
   ```
   
### Setup akun administrator 

<p>Kami telah menyiapkan satu akun level administrator dengan role superadmin, sehingga ia memiliki akses penuh terhadap semua fitur.</p>
<p>Jalankan berikut untuk menyiapkan akun administrator</p>

```sh
   php artisan db:seed
   ```

<!-- USAGE EXAMPLES -->
## Penggunaan Aplikasi

### Login Admin Panel

URL:  { APP_URL }/admin/login <br />
email: superadmin@gosari.com <br />
password: password <br />

### Menu Admin
*  <b> Dashboard </b>
*  <b> Manajemen </b>
	* Dusun
	* Kategori Sampah
	* Target Pelanggan
	* Akun Bank
* <b>Pelanggan</b>
* <b>Tagihan</b>
* <b>Transaksi Pembayaran</b>
* <b>Laporan Transaksi</b>
* <b>Kelola Admin (Hanya dapat di akses super admin)</b>

<!-- LICENSE -->
<h3 id=""> License </h3>

Distributed under the MIT License. See `LICENSE.txt` for more information.
