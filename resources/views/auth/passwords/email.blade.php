@extends('member.landing_page.navbar')
    <section class="forgot-password">
          
        <div class="container text-center">
            <div class="title mt-5">
                <h3>Lupa Password</h3>
            </div>
            <div class="sub-title">
                <p>Kami akan mengirimkan email untuk mengatur ulang kata sandi Anda</p>
            </div>
        </div>
        
            <div class="row justify-content-center">
           
                <div class="col-4 card-forget">
                @if (session('status'))
                        <div class="alert alert-success mt-2" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <form method="POST" action="{{ route('password.email') }}" class="form">
                    @csrf
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label"
                              >Alamat Email</label
                            >
                            <input
                              type="email"
                              class="form-control"
                              id="email"
                              name="email"
                              class= "@error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus
                              placeholder="Masukan Alamat Email"
                            />
                          </div>
                          <button class="btn btn-save" type="submit">Kirim Link Reset Password</button>
                    </form>
                   
                </div>
                
              </div>
    </section>

@extends('member.landing_page.footer')
<style>
  @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;800&display=swap");
* {
  font-family: 'Poppins', sans-serif;
  margin: 0;
  padding: 0;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

.forgot-password {
  padding-bottom:200px;
  padding-top : 150px;
}

.forgot-password .custom-navbar {
  z-index: 5;
  background-color: #08C559;
  -webkit-box-shadow: 0px 10px 20px rgba(199, 199, 199, 0.877);
          box-shadow: 0px 10px 20px rgba(199, 199, 199, 0.877);
}

.forgot-password .custom-navbar .navbar-brand .element3 {
  position: relative;
  z-index: 6;
  width: 200px;
  margin-top: -100px !important;
}

.forgot-password .custom-navbar .navbar-brand img {
  width: 140px;
  position: relative;
}

.forgot-password .custom-navbar .navbar-nav {
  margin-left: 50px;
}

.forgot-password .custom-navbar .navbar-nav .nav-item .nav-link {
  font-size: 15px;
  padding-left: 30px;
  color: white;
  font-weight: 500;
}

.forgot-password .custom-navbar .navbar-nav .nav-item .nav-link:hover {
  color: #00FF6D;
}

.forgot-password .custom-navbar .navbar-nav .nav-item .btn-login {
  color: white;
  -webkit-box-shadow: none !important;
          box-shadow: none !important;
  padding: 10px 20px;
  border-radius: 10px;
  border: #fff 2px solid !important;
  margin-left: 250px;
  z-index: 9;
}

.forgot-password .custom-navbar .navbar-nav .nav-item .btn-login:hover {
  background: #00B54D;
}

.forgot-password .custom-navbar .navbar-nav .nav-item .btn-register {
  padding: 10px 20px;
  border-radius: 10px;
  background-color: white;
  color: #05A102;
  margin-left: 20px;
  position: absolute;
}

.forgot-password .custom-navbar .navbar-nav .nav-item .btn-register:hover {
  background: #f7f7f7;
}

.forgot-password .sub-title p {
  color: #909090;
  margin-top: 20px;
}

.forgot-password .card-forget {
  height: 300px;
  width: 50%;
  background-color: white;
  margin-top: 50px;
  -webkit-box-shadow: 0px 0px 20px rgba(153, 153, 153, 0.418);
          box-shadow: 0px 0px 20px rgba(153, 153, 153, 0.418);
  border-radius: 20px;
}

.forgot-password .card-forget .form {
  padding: 0px 30px;
  margin-top: 60px;
}

.forgot-password .card-forget .form .form-control {
  height: 60px;
  border: none;
  -webkit-box-shadow: 0px 0px 20px rgba(199, 199, 199, 0.418);
          box-shadow: 0px 0px 20px rgba(199, 199, 199, 0.418);
}

.forgot-password .card-forget .form .btn-save {
  background-color: #08C559;
  color: white;
  width: 100%;
  height: 50px;
}
/*# sourceMappingURL=main.css.map */
</style>
