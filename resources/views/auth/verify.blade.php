@extends('layouts.app')

@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/verify-email/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/verify-email/bootstrap.min.css') }}">
    <title>Document</title>
</head>
<body>

    <section class="verify-email">
            <div class="container  text-center">
                <div class="title mt-5">
                    <h3>Verifikasi Email Kamu</h3>
                    <p class="sub-title">Anda perlu memverifikasi email Anda untuk menyelesaikan pendaftaran</p>
                </div>
                <div class="img-verify">
                    <img src="{{asset ('img/verify-email/verify.png') }}" alt="">
                </div>
                <p class="mt-5 about-email">Email telah dikirim ke khoirulanam@gmail.com dengan tautan untuk memverifikasi akun Anda. jika<br> 
                    Anda belum  menerima email setelah beberapa menit, silakan periksa folder spam Anda</p>
                    <div class="button-verify">
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                        <button class="btn resend-email" type="submit">Kirim Ulang Link</button>
                    </form>
                    </div>
                 
                
            </div>
    </section>
    
</body>
</html>

@endsection