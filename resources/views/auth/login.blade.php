<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Go Sari - Unit Pelayanan Kebersihan Lingkungan</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/member/main.css') }}" />
        <link rel="stylesheet" type="text/css"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="shortcut icon" href="{{ asset('img/favicon-icon.png') }}">
    </head>
    <body>
        <section class="login">
            <div class="banner">
                <div class="row g-0">
                    <div class="col-md-12 col-sm-12 col-lg-6 content-banner">
                        <div class="d-flex flex-column align-items-center justify-content-end">
                            <div class="subtitle-banner mt-5">
                                <p>Sub Unit BUMDes Guwosari</p>
                            </div>
                            <div class="title-banner">
                                <p>
                                    Selamat Datang<br />
                                    di Unit Go Sari
                                </p>
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="image-banner">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="{{asset('img/member/auth/plant2.png')}}" alt="" class="plant2" />
                                        <img src="{{asset('img/member/auth/people.png')}}" alt=""
                                            class="image-people" />
                                    </div>
                                    <div class="col-md-6">
                                        <img src="{{asset('img/member/auth/plant.png')}}" alt="" class="plant1" />
                                        <img src="{{asset('img/member/auth/trash.png')}}" alt="" class="image-trash" />
                                    </div>
                                </div>
                                <img src="{{asset('img/member/auth/pedestal.png')}}" alt="" class="image-pedestal" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6 form-login">
                        <img src="{{asset('img/member/auth/line.png')}}" class="line" alt="" />
                        <div class="d-flex flex-column align-items-center">
                            <div class="title">
                                <p>Masuk</p>
                            </div>
                            <div class="sub-title">
                                <p>
                                    Masuk dengan data yang <br />Anda masukkan saat pendaftaran
                                </p>
                            </div>
                        </div>
                        <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                            @csrf
                            <div class="login-banner d-flex align-items-center justify-content-center">
                                <div class="card-login shadow-lg mt-3 bg-body">

                                    @error('email')
                                    <div class="alert alert-danger alert-dismissible show fade">
                                        <div class="alert-body">
                                          Email atau password salah!
                                        </div>
                                      </div>
                                    @enderror
                                    <div class="mb-3">
                                        <label for="email" class="form-label">Alamat Email</label>
                                        <input type="email" name="email" class="form-control" id="email"
                                            placeholder="Masukan Alamat Email" />
                                        </div>
                                    <div class="mb-3">
                                        <label for="password" class="form-label">Password</label>
                                        <input name="password" type="password" class="form-control" id="password"
                                            placeholder="Masukan Password" />
                                        <span toggle="#password"
                                            class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <button class="btn btn-save fw-bold" type="submit">
                                            {{ __('Login') }}
                                        </button>
                                    </div>

                                    <div class="mb-3">
                                        <div class="noAkun text-center">
                                            <p>
                                                Belum punya akun ?
                                                <a href="{{ route('register') }}"
                                                    class="daftarBtn"><span>Daftar</span></a>
                                            </p>
                                            @if (Route::has('password.request'))
                                            <a href="{{ route('password.request') }}" class="forgetBtn">Lupa Password</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <script src="{{ asset('js/member/main.js')}}"></script>
    <script src="{{ asset('js/landing-page/jquery-3.6.0.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".toggle-password").click(function () {
                // alert('k');
                $(this).toggleClass("fa-eye fa-eye-slash");
                let input = $($(this).attr("toggle"));

                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        });
    </script>

    <style>
        body {
            margin: 0px !important;
            padding: 0px !important;
            overflow-x: hidden;
        }

        .toggle-password {
            position: relative;
            z-index: 1;
            left: 90%;
            top: -25px;
            cursor: pointer;
        }
    </style>

</html>
