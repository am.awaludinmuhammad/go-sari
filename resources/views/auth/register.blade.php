@extends('layouts.member.main')
@section('content')
<div class="register">
  <section class="next-register d-flex align-items-center justify-content-center">
    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" class="needs-validation" novalidate="">
      @csrf
      <div class="container">
        <div class="mt-5 mb-5">
          <h3>Pendaftaran Pelanggan Go Sari</h3>
          @error ('password')
              <div class="text-danger">               
               * Password tidak sesuai!
            </div>
          @enderror
          @error ('email')
              <div class="text-danger">               
               * Email sudah pernah digunakan!
            </div>
          @enderror
          @error ('phone')
              <div class="text-danger">               
               * Nomor HP sudah pernah digunakan!
            </div>
          @enderror
          @error ('nik')
              <div class="text-danger">               
               * NIK sudah digunakan!
            </div>
          @enderror
        </div>
        <div class="card border-0 mb-5">
          <div class="my-card-body card-body shadow">
            <div class="row">
              <div class="content-heading">
                <h5 class="ml-3">Informasi Akun</h5>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label for="email" class="form-label">Alamat Email <span
                      class="required">*</span></label>
                  <input required name="email" type="email" class="form-control" id="email"
                    placeholder="Email Aktif dan Benar" value="{{ old('email') }}"/>
                    <div class="invalid-feedback">
                      Harap isikan email!
                    </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label for="password" class="form-label">Password <span
                      class="required">*</span></label>
                  <input name="password" required type="password" minlength="8" class="form-control password-field"
                    id="password" placeholder="Min 8 Karakter (Huruf & Angka)" />
                  <span hidden toggle="#password" id="showPasswordIcon" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                  <div class="invalid-feedback">
                    Password tidak boleh kosong!
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label for="password_confirmation" class="form-label">Ulangi Password <span
                      class="required">*</span></label>
                  <input name="password_confirmation" required type="password" minlength="8"
                    class="form-control password-field" id="password_confirmation"
                    placeholder="Min 8 Karakter (Huruf & Angka)" />
                  <span hidden toggle="#password_confirmation" id="showPasswordConfirmIcon"
                    class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                  <div class="invalid-feedback">
                    Konfirmasi password tidak boleh kosong!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card border-0 mb-5">
          <div class="my-card-body card-body shadow">
            <div class="row mb-3">
              <div class="content-heading">
                <h5 class="ml-3">Data Pribadi</h5>
              </div>
              <div class="row mb-3">
                <div class="col-md-4 col-sm-12">
                  <div class="form-group mb-3">
                    <label for="name" class="form-label">Nama Lengkap <span
                        class="required">*</span></label>
                    <input required name="name" type="text" class="form-control" id="name"
                      placeholder="Nama Lengkap" value="{{ old('name') }}" />
                    <div class="invalid-feedback">
                      Nama tidak boleh kosong!
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12">
                  <div class="form-group mb-3">
                    <label for="phone" class="form-label">No. HP <span class="required">*</span></label>
                    <input required name="phone" type="text" onkeypress="return onlyNumberKey(event)" class="form-control" maxlength="16" minlength="10" id="phone"
                      placeholder="No Hp harus aktif dan benar" value="{{ old('phone') }}"/>
                    <div class="invalid-feedback">
                      No. HP tidak boleh kosong!
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12">
                  <div class="form-group mb-3">
                    <label for="rt" class="form-label">RT <span class="required">*</span></label>
                    <select class="form-control  @error('rt') is-invalid @enderror" id="rt" name="rt">
                      @foreach(range(1,10) as $rt)
                      <option value="{{ sprintf("%02d", $rt) }}">{{ sprintf("%02d", $rt) }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-md-4 col-sm-12">
                  <div class="form-group mb-3">
                    <label for="nik" class="form-label">NIK <span class="required">*</span></label>
                    <input required type="text" minlength="1" maxlength="16" onkeypress="return onlyNumberKey(event)" maxlength="11" name="nik" class="form-control" id="nik"
                      placeholder="16 Digit NIK" value="{{ old('nik') }}"/>
                    <div class="invalid-feedback">
                      NIK tidak boleh kosong!
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12">
                  <div class="form-group mb-3">
                    <label for="hamlet" class="form-label">Dusun <span class="required">*</span></label>
                    <select class="form-control" id="hamlet" name="hamlet_id">
                      @foreach ($hamlets as $hamlet)
                      <option value="{{ $hamlet->id }}">{{ $hamlet->hamlet_name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12">
                  <div class="form-group mb-3">
                    <label for="address" class="form-label">{{ __('Alamat Lengkap') }} <span
                        class="required">*</span></label>
                    <textarea name="address" class="form-control @error('address') is-invalid @enderror"
                      id="address" rows="3" placeholder="Cth. Dusun Genteng RT 02, Guwo Sari">{{ old('address') }}</textarea>
                    <div class="invalid-feedback">
                      Alamat tidak boleh kosong!
                    </div>
                  </div>
                </div>
              </div>
            <div>
            <div class="row mb-5">
              <div class="content-heading">
                <h5 class="ml-3">Kriteria Berlangganan</h5>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="form-group mb-3">
                  <label for="garbage_category_id" class="form-label">Kategori Sampah <span
                      class="required">*</span></label>
                  <select class="form-control" id="garbage_category_id" name="garbage_category_id">
                    @foreach ($garbageCategories as $category)
                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="form-group mb-3">
                  <label for="garbage_can_location" class="form-label">Letak Tong Sampah <span
                    class="required">*</span> </label>
                  <select class="form-control" id="garbage_can_location" name="garbage_can_location">
                    <option value="depan">Depan</option>
                    <option>Belakang</option>
                    <option>Samping</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="form-group mb-3">
                  <label for="garbage_can_picture" class="form-label">Upload Foto Rumah / Tempat Sampah<span
                      class="required">*</span> </label>

                  <input name="garbage_can_picture" type="file" class="form-control"
                    id="garbage_can_picture" required/>
                  <div class="invalid-feedback">
                    Foto tidak boleh kosong!
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="content-heading">
                <h5>Syarat dan Ketentuan</h4>
              </div>
              <div class="col-12">
                <div class="mb-3">
                  <ol>
                    <li>Bersedia untuk memisah sampah sesuai jenisnya.</li>
                    <li>
                      Tidak membuang sampah diluar kategori sampah, contoh (sofa, kasur, bantal, pangkasan pohon, batu, krikil dan sejenisnya).
                    </li>
                    <li>Tempo pembayaran sebelum tanggal 10 setiap bulannya.</li>
                    <li>Tidak memberi tips (fee) kepada petugas saber.</li>
                    <li>
                      Semua transaksi harus berkwitansi Go Sari Pesan anda akan
                      kami tanggapi dalam waktu 1x24 jam pada jam kerja.
                    </li>
                    <li>
                    Bersedia menyiapkan 2 - 3 tempat sampah (misal: ember,
                    bagor, kantong plastik dan sejenisnya).
                    </li>
                    <li>
                      Jika melanggar
                      kesepakatan pelayanan pengolahan go sari maka saya bersedia
                      ditegur, ditunda pengambilan sampah, dan diberhentikan
                      kerjasamanya.
                    </li>
                  </ol>
                </div>

                <div class="form-check d-flex justify-content-center mb-3">
                  <div>
                    <input class="form-check-input" type="checkbox" id="agree">
                    <label class="ml-2 form-check-label" for="agree">
                      Setuju dan Bersedia sesuai ketentuan di atas
                    </label>
                  </div>
                </div>
                <div class="mb-3 d-flex align-items-center justify-content-center button">
                  <a href="{{ url('/') }}" class="btn btn-cancel" type="button">Kembali</a>
                  <button id="regBtn" class="btn btn-save" type="submit" disabled>
                    {{ __('Daftar') }}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>      
      </div>
    </form>
  </section>
</div>
@endsection

@section('script')
<script>
  function onlyNumberKey(evt) {
      // Only ASCII character in that range allowed
      var ASCIICode = (evt.which) ? evt.which : evt.keyCode
      if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
          return false;
      return true;
  }
$(document).ready(function () {
  $('#tanggal').datepicker({
    format: 'yyyy-mm-dd',
    daysOfWeekDisabled: "0",
    autoclose: true
  });

  // show eye icon after on focus
  $('.password-field').focus(function() {
    let id = $(this).attr('id');
    
    ( id === "password" ) ? $('#showPasswordIcon').removeAttr('hidden') : $('#showPasswordConfirmIcon').removeAttr('hidden');
  });

  $(".toggle-password").click(function () {
    $(this).toggleClass("fa-eye-slash fa-eye");
    
    let toggle = $(this).attr("toggle");
    let input = $(toggle);

    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });

  $("#agree").change(function () {
    let agree = $('#agree').prop('checked');
    if (agree === true) {
      $("#regBtn").removeAttr("disabled", "disabled");
      $(".form-check-label").addClass("text-success");
    } else {
      $('#regBtn').attr('disabled', 'disabled');
      $(".form-check-label").removeClass("text-success");
    }
  });
});
</script>
@endsection

@section('style')
<style>
  @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;800&display=swap");


  * {
    font-family: "Poppins", sans-serif;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  .toggle-password {
    position: relative;
    z-index: 1;
    left: 90%;
    top: -30px;
    cursor: pointer;
  }

  .next-register {
    margin-top: 100px;
    margin-bottom: 100px;
    background-size: cover;
  }


  .title {
    margin-bottom: -50px;
    color: #303030;
  }

  .account-info {
    margin-top: 100px;
    background-color: #fff;
    margin-bottom: 50px;
    border-radius: 20px;
    padding: 20px 30px;
  }

  .personal-data {
    padding: 0px 50px;
  }

  .content-heading {
    margin-bottom: 10px;
  }

  .form-control::-webkit-input-placeholder {
    font-size: 14px;
    color: #A1A1A1;
    font-weight: 300;
    outline: none;
    letter-spacing: 1px;

  }

  .form-control:focus {
    border: 1px solid #08c559 !important;
    -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0) inset !important;
    -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0) inset !important;

    outline: none !important;
  }

  .card-next-register {
    padding-top: 50px;
    padding-left: 30px;
    padding-right: 30px;
    background-color: #fff;
    border-radius: 20px;
    padding-bottom: 50px;

  }

  .my-card-body {
    border-radius: 15px;
    background-color: #fff;
    padding: 30px 30px;
  }

  .required {
    color: red;
  }

  .btn-save {
    background-color: #08c559 !important;
    color: white !important;
    width: 150px;
  }

  .btn-save:hover {
    background-color: #2ECC71 !important;
    color: white !important;
    width: 150px;
  }

  .btn-cancel {
    border: 1px #adb5bd solid !important;
    color: #9ca2a5 !important;
    width: 150px;
    margin-right: 10px;
  }

  .btn-cancel:hover {
    border: 1px #adb5bd solid !important;
    color: #ffff !important;
    width: 150px;
    background-color: #c7c7c7 !important;
  }


  .convention ol li {
    font-size: 14px;
  }

  .convention p {
    font-size: 14px;
  }

  .convention {
    padding: 0px 20px;
  }
</style>
@endsection