@extends('layouts.member.dashboard.main')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Transaksi</h1>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card invoice-history">
                    <div class="card-header">
                        <h4>Riwayat Transaksi</h4>
                        <!-- <div class="card-header-action">
                            <a href="#" class="btn btn-info">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                        </div> -->
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive table-invoice">
                            <table class="table table-striped">
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Kode</th>
                                    <th>Pelanggan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                                @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ date('d-m-Y', strtotime($transaction->payment_date)) }}</td>
                                    <td><a href="#">{{ $transaction->code }}</a></td>
                                    <td class="font-weight-600">{{ $transaction->user_name }}</td>
                                    <td>
                                        @if($transaction->payment_status == "pending")
                                        <span class="badge badge-warning">Menunggu</span>
                                        @else
                                        <span class="badge badge-success">Lunas</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('invoices.detail', $transaction->code) }}" class="btn btn-primary">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
@endsection