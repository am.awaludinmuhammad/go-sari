<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/member/payment/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon-icon.png') }}">
    <title>Document</title>
</head>
<body>
    <section class="section-success">
            <div class="brand text-center">
                <p>Go <span>Sari</span></p>
                <hr>
            </div>
            <div class="title text-center mb-5">
                <h3>Pembayaran Berhasil</h3>
            </div>

            <div class="img-success text-center">
            <img src="{{ asset('img/member/payment-invoice/success-pay.png') }}" alt="">
            </div>

            <div class="text-confirm text-center">
                <p>Nanti akan kami informasikan <br>melalui email
                    setelah transaksi diterima</p>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary btn-pay">Back To Dashboard</button>
            </div>
           
    </section>
</body>
</html>