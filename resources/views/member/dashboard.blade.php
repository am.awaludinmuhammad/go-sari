@extends('layouts.member.dashboard.main')
@section('content')
@section('style')
<style>
  #resendBtn {
    background: none; 
    border: none; 
    padding: 0;
    color: #262626;
    text-decoration: underline;
  }
  #resendBtn:hover {
    color: #fafafa;
  }
</style>
@endsection
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    @if(!$user->email_verified_at)
    <div class="verify-warning mb-3 text-center">
      <div class="alert alert-warning">
        Kami telah mengirimkan kode aktivasi. Silakan cek email untuk melakukan verifikasi. 
        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
          @csrf
          <button id="resendBtn" type="submit">Kirim Ulang Link</button>
        </form>
      </div>
    </div>
    @endif
    <div class="row ">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <div class="card card-profil">
        
          <div class="row">
            <div class="col-4">
              <div class="img-profil">
                @if($user->profile_picture == "avatar-default.png")
                <img src="{{ asset('img/avatar-default.png') }}" class="img-fluid element1" alt="" />
                @else
                <img src="{{ asset('img/member/avatar/'.$user->profile_picture) }}" class="img-fluid element1" alt="" />
                @endif
              </div>
            </div>
            <div class="col-8">
              
              <div class="name">
                <h4>{{ $user->name }}</h4>
              </div>
              <div class="email">
                <p>{{ $user->email }}</p>
              </div>
              <div class="phone">
                <p>{{ $user->phone }}</p>
              </div>
              <div class="address">
                <p>{{ $user->address }}</p>
              </div>
              @if($user->status == '1')
                <span class="badge badge-active badge-success">Aktif</span>
              @else
                <span class="badge badge-nonActive badge-danger">Belum Aktif</span>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        @if(!empty($currentInvoice))
        <div class="card card-invoice">
          <div class="invoice-date">
            <p>Tagihan | {{ $invoiceMonth }}</p>
          </div>
          <div class="invoice-price">
            <p>Rp {{ number_format($currentInvoice->total_amount) }}</p>
          </div>
          <div class="invoice-dedline">
            <p>Batas pembayaran: 
            {{ date('d-m-Y, H:i',strtotime($currentInvoice->payment_due)) }} WIB</p>
          </div>
          <div class="invoice-status">
            <div class="d-flex flex-row bd-highlight mb-3">
              <div class="bedges">
                @if($currentInvoice->payment_status == "unpaid")
                  <span class="badge badge-unpaid badge-danger">Belum Lunas</span>
                @elseif($currentInvoice->payment_status == "pending")
                  <span class="badge badge-paid badge-warning">Menunggu</span>
                @else
                  <span class="badge badge-paid badge-success" style="background:white !important; color:#08C559">Lunas</span>
                @endif
              </div>
              @if($currentInvoice->payment_status == "unpaid")
                <a href="{{ url('payment', $currentInvoice->code) }}" class="btn btn-more">Bayar</a>
              @endif
            </div>
          </div>
        </div>
        @else
        <div class="card card-invoice">
          <div class="invoice-date">
            <p>Tagihan | {{ $invoiceMonth }}</p>
          </div>
          <div class="invoice-price">
            <p>Rp. 0</p>
          </div>
          <div class="invoice-dedline">
            <p>Batas pembayaran: -</p>
          </div>
          <div class="invoice-status">
            <div class="d-flex flex-row bd-highlight mb-3">
              <div class="bedges">
                <span class="badge badge-unpaid badge-warning">Tagihan Belum Tersedia</span>
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
      <div class="card invoice-history">
        <div class="card-header">
          <h4>Riwayat Transaksi</h4>
          <div class="card-header-action">
            <a href="{{ url('transactions') }}
            " class="btn btn-info">Selengkapnya <i class="fas fa-chevron-right"></i></a>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>Tanggal</th>
                <th>Kode</th>
                <th>Pelanggan</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
              @foreach($transactions as $transaction)
              <tr>
                <td>{{ date('d-m-Y', strtotime($transaction->payment_date)) }}</td>
                <td><a href="#">{{ $transaction->code }}</a></td>
                <td class="font-weight-600">{{ $transaction->user->name }}</td>
                <td>
                @if($transaction->payment_status == "pending")
                  <span class="badge badge-warning">Menunggu</span>
                @else
                  <span class="badge badge-success">Lunas</span>
                @endif
                </td>
                <td>
                  <a href="{{ route('invoices.detail', $transaction->code) }}" class="btn btn-primary">Detail</a>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
      <div class="col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="calendar">
          <div class="calendar-header">
            <span class="month-picker" id="month-picker">February</span>
            <div class="year-picker">
              <span class="year-change" id="prev-year">
                <
              </span>
              <span id="year">2021</span>
              <span class="year-change" id="next-year">
                >
              </span>
            </div>
          </div>
          <div class="calendar-body">
            <div class="calendar-week-day">
              <div>Min</div>
              <div>Sen</div>
              <div>Sel</div>
              <div>Rab</div>
              <div>Kam</div>
              <div>Jum</div>
              <div>Sab</div>
            </div>
            <div class="calendar-days"></div>
          </div>

          <div class="month-list"></div>
        </div>
      </div>
    </div>

  </section>
</div>
@endsection
@section('script')

<script>

  
</script>
  @if (Session::has('success'))
    <script>
      iziToast.success({
        position: 'topRight',
        title: 'Sukses',
        message: '{{ Session::get("success") }}',
        timeout: 3000
      });
    </script>
  @endif
  @if (Session::has('error'))
    <script>
      iziToast.error({
        position: 'topRight',
        title: 'Error',
        message: '{{ Session::get("error") }}',
        timeout: 3000
      });
    </script>
  @endif
@endsection