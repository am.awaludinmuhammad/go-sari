@extends('layouts.app')

@section('content')
<div class="container">
  <div class="table-responsive">
    <table class="table table-striped table-bordered" id="invoiceTable">
      <thead>
        <tr>
          <th class="text-center">
            #No
          </th>
          <th>Invoice Code</th>
          <th>Bulan</th>
          <th>Batas Pembayaran</th>
          <th>Total Tagihan</th>
          <th>Status Pembayaran</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach($invoices as $invoice)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $invoice->code }}</td>
          <td>{{ $invoice->month }}</td>
          <td>{{ date('d M Y', strtotime($invoice->payment_due)) }}</td>
          <td>Rp. {{ number_format($invoice->total_amount) }}</td>
          <td>{{ $invoice->payment_status }}</td>
          <td>
            <a href="{{route('invoices.detail', $invoice->code)}}" class="btn btn-sm btn-success mr-1"
              data-toggle="tooltip" title="Detail">Detail</a>
            <a href="{{route('invoices.upload', $invoice->code)}}" class="btn btn-sm btn-success mr-1"
              data-toggle="tooltip" title="Detail">BAYAR</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
