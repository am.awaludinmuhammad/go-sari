<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing-page/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing-page/main.css') }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css"
    href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <title>Document</title>
</head>
<body>
 <!-- Footer Start -->
 <footer>
      <div class="container">
        <div class="row ">
          <div class="col-md-3 col-lg-4">
            <!-- footer about -->
            <div class="footer-about">
              <div class="footer-about__img mb-5">
                <img src="{{ asset('img/landing-page/logo.png') }}" class="img-fluid footer-logo" alt="" />
              </div>
              <p>
                Badan Usaha Milik Desa Guwosari Maju, berdiri sejak tahun 2016 (Perdes No.5 Tahun 2016)
              </p>
              <p>©Bumdes Guwosari Maju 2021. All rights reserved</p>
            </div>
          </div>
          <div class="col-md-9 col-lg-3 account">
            <p class="footer-title">Rekening</p>
            <ul>
              <li>
                <p>A/n Bumdes Guwosari Maju</p>
              </liz>
              <li>
                <img src="{{ asset('img/landing-page/BRI.png') }}" alt=""> : 1117991898
              </li>
              <li>
                <img src="{{ asset('img/landing-page/BNI.png') }}" alt=""> :  6635-01-018610-53-5
              </li>
              <li>
                <p>NPWP : 84.439.984.0-543.000
                </p>
              </li>
            </ul>
          </div>
          <div class="col-md-3 col-lg-2">
            <p class="footer-title">Social Media</p>
            <div class="social-media">
              <ul>
                <li><a href="https://www.instagram.com/bumdesguwosari/"><i class="fab fa-instagram"></i></a><a href="https://web.facebook.com/Bumdes-Guwosari-Maju-258154138788074?_rdc=1&_rdr"><i class="fab fa-facebook ms-4"></i></a></li>
                <li></li>
                <li><a href="https://twitter.com/bumdesguwosari"><i class="fab fa-twitter"></i></a><a href="https://www.youtube.com/channel/UCMNgJ47OYRdqXK3vPKnLhvA?view_as=subscriber"><i class="fab fa-youtube ms-4"></i></a></li>
                <li></li>
              </ul>
            </div>
          </div>

          <div class="col-md-2 col-lg-2">
            <p class="footer-title">Address</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1011752.4030018495!2d110.314286!3d-7.87855!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbcb118c4959c7459!2sBumdes%20Guwosari%20Maju!5e0!3m2!1sid!2sid!4v1623407292262!5m2!1sid!2sid" width="300" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
          </div>
        </div>
      </div>
    </footer>
    </body>
</html>
 <script src="https://kit.fontawesome.com/abae7fb329.js" crossorigin="anonymous"></script>
