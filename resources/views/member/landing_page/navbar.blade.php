<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
        <title>Document</title>
    </head>

    <body>
        <section class="ladingPage">
            <nav class="navbar navbar-expand-lg navbar-light custom-navbar fixed-top" data-aos="fade-right">
                <div class="container">
                    <a class="navbar-brand" href="{{ route('landing') }}">
                        <img src="{{asset('img/landing-page/brand.png')}}" class="img-fluid img-brand" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="{{ route('landing') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://guwosarimaju.id/">Guwosari</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact</a>
                            </li>
                            @if (Route::has('login'))
                            @auth
                            <!-- <a href="{{ url('/home') }}">Home</a> -->
                            @else
                            <li class="nav-item">
                                <a href="{{ route('login') }}"><button type="button"
                                        class="btn btn-login">Masuk</button></a>
                            </li>
                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a href="{{ route('register') }}"><button type="button"
                                        class="btn btn-register">Daftar</button></a>
                            </li>
                            @endif
                            @endauth
                            @endif

                        </ul>
                    </div>
                </div>
            </nav>
    </body>

</html>

<style>
    @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;800&display=swap");

    body {
        margin: 0px !important;
        padding: 0px !important;
        overflow-x: hidden;
    }

    * {
        font-family: 'Poppins', sans-serif;
        margin: 0;
        padding: 0;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    .shrink .custom-navbar {
        z-index: 6;
        -webkit-box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
        box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
        background: #fff;
        -webkit-animation: fadeInDown;
        animation: fadeInDown;
        /* referring directly to the animation's @keyframe declaration */
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
    }

    .shrink .custom-navbar .btn-register {
        padding: 10px 20px;
        border-radius: 10px;
        border: 1px solid #05A102;
        color: #05A102;
        margin-left: 20px;
        position: absolute;
    }

    .shrink .custom-navbar .btn-register:hover {
        background: #f7f7f7;
    }

    .section-line {
        width: 60px;
        height: 2px;
        background: #05A102;
        margin: 30px auto;
        display: block;
    }

    .custom-navbar {
        z-index: 5;
        -webkit-box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
        box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
        background: #fff;
    }

    .custom-navbar .navbar-brand .element3 {
        position: relative;
        z-index: 6;
        width: 200px;
        margin-top: -100px !important;
    }

    .custom-navbar .navbar-brand img {
        width: 200px;
        position: relative;
    }

    .custom-navbar .navbar-nav {
        margin-left: 50px;
    }

    .custom-navbar .navbar-nav .nav-item .nav-link::before {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 0;
        height: 2px;
        -webkit-transition: width 0.2s ease-out;
        transition: width 0.2s ease-out;
        background-color: #05A102;
    }

    .custom-navbar .navbar-nav .nav-item .nav-link {
        font-size: 15px;
        position: relative;
        padding: 10px 20px;
        color: #05A102;
        font-weight: 500;
    }

    .custom-navbar .navbar-nav .nav-item .nav-link:hover::before {
        width: 100%;
    }

    .custom-navbar .navbar-nav .nav-item .btn-login {
        background: -webkit-gradient(linear, left top, right top, from(#00B54D), to(#00FF6D));
        background: linear-gradient(to right, #00B54D, #00FF6D);
        color: white;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        padding: 10px 20px;
        border-radius: 10px;
        border: none !important;
        margin-left: 250px;
        z-index: 9;
    }

    .btn:focus {
        border: 1px solid #08c559 !important;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0) inset !important;
        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0) inset !important;
        outline: none !important;
    }

    .custom-navbar .navbar-nav .nav-item .btn-login:hover {
        background: #00B54D;
    }

    .custom-navbar .navbar-nav .nav-item .btn-register {
        padding: 10px 20px;
        border-radius: 10px;
        border: 1px solid #05A102;
        background-color: white;
        color: #05A102;
        margin-left: 20px;
        position: absolute;
    }

    .custom-navbar .navbar-nav .nav-item .btn-register:hover {
        background: #f7f7f7;
    }
</style>