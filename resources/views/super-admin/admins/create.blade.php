@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Kelola Admin</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ url('admin/super-admin/admins') }}">Data Admin</a></div>
        <div class="breadcrumb-item">Tambah Admin</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <form method="post" action="{{ url('super-admin/admins') }}" class="needs-validation" novalidate="">
              @csrf
              <div class="card-header">
                <h4>Tambah Admin</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="name" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>No HP/Whatsapp</label>
                  <input type="text" id="phoneInput" class="form-control" name="phone" onkeypress="return onlyNumberKey(event)" required="" placeholder="Cth. 6281XXX">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                  <span class="err-msg text-danger mt-1"></span>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select class="form-control" name="role" required="">
                      <option selected value="">Pilih Role ...</option>
                      
                      <option value="admin">Admin</option>
                      <option value="superadmin">Super Admin</option>
                      
                    </select>
                    <div class="invalid-feedback">
                      Isian masih kosong
                    </div>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Ulangi Password</label>
                  <input type="password" class="form-control" name="password_confirmation" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>

                <div class="card-footer text-right">
                  <button class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script>
  // Allow only number input
  function onlyNumberKey(evt) {
    // Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
  }
  $(document).ready(function(){
    $("#phoneInput").change(function(){
        let inputField = $(this);
        let twoFirstNumber = inputField.val().substr(0,2);
        let errorMsg = $(".err-msg");
        if(twoFirstNumber !== "62") {
          errorMsg.text("* Gunakan kode negara Indonesia (62). Contoh 6281XXX");
        }
    });
  });
</script>
@endsection