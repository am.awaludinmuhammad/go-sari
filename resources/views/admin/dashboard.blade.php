@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    <div class="section-body">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="card card-statistic-2" style="height: 230px">
            <div class="card-stats mt-3">
              <div class="card-stats-title">Tagihan -
                <div class="dropdown d-inline">
                  <span class="font-weight-600 text-primary">{{ $invoiceMonth }}</span>
                </div>
              </div>
              <div class="card-stats-items">
                <div class="card-stats-item">
                  <div class="card-stats-item-count text-warning">{{$invoicePending}}</div>
                  <div class="card-stats-item-label text-warning">Menunggu</div>
                </div>
                <div class="card-stats-item">
                  <div class="card-stats-item-count text-danger">{{$invoiceUnpaid}}</div>
                  <div class="card-stats-item-label text-danger">
                    <span>Blm </span><span>Lunas</span>
                  </div>
                </div>
                <div class="card-stats-item">
                  <div class="card-stats-item-count text-success">{{$invoicePaid}}</div>
                  <div class="card-stats-item-label text-success">Lunas</div>
                </div>
              </div>
            </div>
            <div class="card-icon shadow-primary bg-primary" style="margin-top: 40px">
              <i class="fas fa-archive"></i>
            </div>
            <div class="card-wrap mt-3">
              <div class="card-header">
                <h4>Total Tagihan</h4>
              </div>
              <div class="card-body">
                {{ $invoiceTotal }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-primary">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Pambayaran Masuk</h4>
                  </div>
                  <div class="card-body">
                    Rp {{number_format($paidAmount)}}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card card-statistic-2 tes">
                <div class="card-icon shadow-primary bg-primary">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Pembayaran Belum Masuk</h4>
                  </div>
                  <div class="card-body">
                    Rp {{number_format($unpaidAmount)}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
          <div class="col-md-6 col-sm-12">
              <div class="card">
                  <div class="card-header">
                      <h4>Jumlah Pelanggan</h4>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table table-bordered table-md">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Dusun</th>
                                  <th>Jumlah Pelanggan</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach ($hamletMembersCount as $hamlet)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$hamlet->hamlet_name}}</td>
                                    <td>{{$hamlet->total_member}}</td>
                                </tr>
                                @endforeach
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th colspan="2">Total</th>
                                  <th>{{ $totalMembers }}</th>
                                </tr>
                              </tfoot>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-6 col-sm-12">
              <div class="card">
                  <div class="card-header">
                      <h4>Target Pelanggan</h4>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table table-bordered table-md">
                            <thead>
                              <tr>
                                  <th>No</th>
                                  <th>Dusun</th>
                                  <th>Target Pelanggan</th>
                                  <th>Pencapaian</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($hamletMembersCount as $target)
                              <tr>
                                  <td>{{$loop->iteration}}</td>
                                  <td>{{$target->hamlet_name}}</td>
                                  <td>{{$target->target_qty}}</td>
                                  <td>{{number_format($target->percentage, 2)}} %</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
@if (Session::has('error'))
<script>
iziToast.error({
  position: 'topRight',
  title: 'Error',
  message: '{{ Session::get("error") }}',
  timeout: 5000
});
</script>
@endif
@endsection
