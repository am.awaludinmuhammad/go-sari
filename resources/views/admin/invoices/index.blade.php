@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Invoice</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Invoice</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>Data Invoice</h4>
            </div>
            <div class="card-body">
              <div class="mb-3">
                <a href="{{ route('invoices.create') }}" class="btn btn-sm btn-primary mr-1 mb-3"><i
                    class="fas fa-plus"></i> Buat Invoice</a>
              </div>             
              <div class="card">
                <div class="card-body">
                  <div class="row mb-1">
                    <ul class="nav nav-pills" id="myTab1" role="tablist">
                      @foreach($months as $m)
                      <li class="nav-item">
                        <a class="nav-link filterMonth" data-toggle="tab" data-month="{{ $m }}" href="javascript:void(0)" role="tab" aria-selected="false">{{ $m }}</a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                  <div class="mb-3">
                    <a href="javascript:void(0)" title="Reset Filter" id="reset" class="btn btn-sm btn-light">Reset</a>
                  </div>
                  <div class="mb-3">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active status-tab" id="all" data-status="all" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="true">Semua (<span id="countAll"></span>)</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link status-tab" id="unpaid" data-status="unpaid" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="false">Belum Lunas (<span id="countUnpaid"></span>)</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link status-tab" id="paid" data-status="paid" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="false">Lunas (<span id="countPaid"></span>)</a>
                      </li>
                    </ul>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="invoiceTable">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Bulan</th>
                          <th>Kode</th>
                          <th>Member</th>
                          <th>Jumlah Tagihan</th>
                          <th>Batas Pembayaran</th>
                          <th>Status</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
@if (Session::has('success'))
<script>
iziToast.success({
  position: 'topRight',
  title: 'Sukses',
  message: '{{ Session::get("success") }}',
  timeout: 3000
});
</script>
@endif
@if (Session::has('error'))
<script>
iziToast.error({
  position: 'topRight',
  title: 'Error',
  message: '{{ Session::get("error") }}',
  timeout: 3000
});
</script>
@endif

<script>
  $(document).ready(function() {
    function deleteInvoice(id, status) {
      swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan menghapus data invoice pelanggan.',
        icon: 'warning',
        dangerMode:true,
        buttons: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "post",
            dataType: "json",
            url: '/admin/invoices/delete/'+id,
            data: {
              '_token': "{{ csrf_token() }}",
              'id':id
            },
            success: function(data) {
              getCountData();

              table.ajax.url("/admin/invoices/data?status="+status).load();
            }
          });
          swal('Data berhasil dihapus.', {
            icon: 'success',
          });
        } else {
          swal('Data batal dihapus.');
        }
      });
    }

    function confirmPayment(id, status) {
      swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan mengkonfirmasi pembayaran pelanggan.',
        icon: 'warning',
        dangerMode:true,
        buttons: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "post",
            dataType: "json",
            url: '/admin/invoices/confirm/'+id,
            data: {
              '_token': "{{ csrf_token() }}",
              'id':id
            },
            success: function(data) {
              getCountData();

              table.ajax.url("/admin/invoices/data?status="+status).load();
            }
          });
          swal('Konfirmasi Pembayaran Berhasil.', {
            icon: 'success',
          });
        } else {
          swal('Konfirmasi dibatalkan.');
        }
      });
    }

    function getCountData() {
      $.get("/admin/invoices/count", function( data ) {
        $('#countAll').text(data.all);
        $('#countUnpaid').text(data.unpaid);
        $('#countPaid').text(data.paid);
      });
    }

    getCountData();

    // invoice table
    let table = $('#invoiceTable').DataTable({
      "ajax": "/admin/invoices/data?status=all",
      "columns":[
        {"data": "id"},
        {"data": "month"},
        {
          "data": null,
          "render": function (data, type, row) {
            return data.code;
          }
        },
        {"data": "name"},
        {
          "data": "total_amount",
          "render": function(amount) {
            return 'Rp. ' + amount;
          }
        },
        {"data": "payment_due"},
        {
          "data": "payment_status",
          "render": function(status) {
            if (status == "unpaid") {
              return '<span class="badge badge-pill badge-danger">Blm Lunas</span>'
            } else if (status == "pending") {
              return '<span class="badge badge-pill badge-warning">Menunggu</span>'
            } else {
              return '<span class="badge badge-pill badge-success">Lunas</span>'
            }
          }
        },
        {
          "data": null,
          "render": function (data, type, row) {
            if (data.payment_status === 'unpaid') {
              return '<a href="/admin/invoices/'+ encodeURIComponent(data.code) +'" class="btn btn-sm btn-info mr-1" data-toggle="tooltip" title="Lihat Detail"><i class="fas fa-info-circle"></i></a><button id="btn-confirm" class="btn btn-sm btn-warning" data-id="'+data.id+'" data-toggle="tooltip" title="Konfirmasi Manual"><i class="fas fa-check"></i></button> <button class="btn btn-sm btn-danger mr-1 delete" data-id="'+data.id+'" data-toggle="tooltip" title="Hapus"><i class="fas fa-trash"></i></button>';
            } else {
              return '<a href="/admin/invoices/'+ encodeURIComponent(data.code) +'" class="btn btn-sm btn-info mr-1" data-toggle="tooltip" title="Lihat Detail"><i class="fas fa-info-circle"></i></a> <button class="btn btn-sm btn-danger delete" data-id="'+data.id+'" data-toggle="tooltip" title="Hapus"><i class="fas fa-trash"></i></button>';
            }
          }
        }
      ]
    });

    // Add increment no. in first column
    table.on( 'draw.dt', function () {
      let PageInfo = $('#invoiceTable').DataTable().page.info();
          table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

    // month filter
    $('.filterMonth').click(function() {
      let date = new Date();
      let year = date.getFullYear();
      let month = $(this).data('month');

      table.search(month + ' ' + year).draw();
    });

    // reset month filter
    $('#reset').click(function() {
      $('.filterMonth').removeClass('active');
      table.search('').draw();
    });

    // delete invoice
    $('#invoiceTable tbody').on('click', '.delete', function () {
      let status = $( "a.status-tab[aria-selected='true']" ).data('status');
      let id = $(this).data('id');

      deleteInvoice(id, status);
    });

    // manual confirmation
    $('#invoiceTable tbody').on('click', '#btn-confirm', function () {
      let status = $( "a.status-tab[aria-selected='true']" ).data('status');
      let id = $(this).data('id');
      
      confirmPayment(id, status);
    });

    // invoice status filter
    $('.status-tab').click(function() {
      let status = $(this).data('status');
      table.ajax.url("/admin/invoices/data?status="+status).load();
    });
  });
</script>
@endsection
