@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Transaksi Pembayaran</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Transaksi Pembayaran</div>
      </div>
    </div>

    <div class="section-body">

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Data Pembayaran</h4>
            </div>
            <div class="card-body">
              <div class="row mb-1">
                <ul class="nav nav-pills" id="myTab1" role="tablist">
                  @foreach($months as $m)
                  <li class="nav-item">
                    <a class="nav-link filterMonth" data-toggle="tab" data-month="{{ $m }}" href="javascript:void(0)" role="tab" aria-selected="false">{{ $m }}</a>
                  </li>
                  @endforeach
                </ul>
              </div>
              <div class="mb-3 mt-2">
                <a href="javascript:void(0)" title="Reset Filter" id="reset" class="btn btn-light">Reset</a>
              </div>
              <div class="mb-3">
                <ul class="nav nav-tabs" id="myTab2" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active status-tab" id="all" data-status="all" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="true">Semua (<span id="countAll"></span>)</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link status-tab" id="unprocessed" data-status="unprocessed" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="false">Belum Diproses (<span id="countUnprocessed"></span>)</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link status-tab" id="processed" data-status="processed" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="false">Sudah Diproses (<span id="countProcessed"></span>)</a>
                  </li>
                </ul>
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-bordered" id="transactionTable">
                  <thead>
                    <tr>
                      <th class="text-center">No.</th>
                      <th class="text-center">Bulan</th>
                      <th class="text-center">Nama</th>
                      <th class="text-center">Dusun</th>
                      <th class="text-center">RT</th>
                      <th class="text-center">Kategori</th>
                      <th class="text-center">Harga</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Bukti Pembayaran</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    function confirmPayment(id, status) {
      swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan mengubah status pembayaran',
        icon: 'warning',
        buttons: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "post",
            dataType: "json",
            url: '/admin/transactions/change-status',
            data: {
              "_token": "{{ csrf_token() }}",
              "id": id
            },
            success: function() {
              getCountData();
              // reload table data
              $('#transactionTable').DataTable().ajax.url("/admin/transactions-data?status="+status).load();
            }
          });
          swal('Status pembayaran berhasil dirubah.', {
            icon: 'success',
          });
        } else {
          swal('Status pembayaran tidak dirubah.');
        }
      });
    }

    function getCountData() {
      $.get( "/admin/transactions/count", function( data ) {
        $('#countAll').text(data.all);
        $('#countUnprocessed').text(data.unprocessed);
        $('#countProcessed').text(data.processed);
      });
    }

    // get total transaction
    getCountData()

    let table = $('#transactionTable').DataTable({
      "ajax": "/admin/transactions-data?status=all",
      "columns":[
        {"data": "id"},
        {"data": "month"},
        {"data": "name"},
        {"data": "hamlet_name"},
        {"data": "rt"},
        {"data": "category_name"},
        {"data": "total_amount",
          "render": function (amount) { return "Rp. " + new Intl.NumberFormat().format(amount);}
        },
        { "data": "payment_status",
          "render": function (status) {
            if (status === "pending") {
              return '<span class="badge badge-warning">Menunggu</span>';
            } else {
              return '<span class="badge badge-success">Lunas</span>';
            }
          }
        },
        {
          "data": null,
          "render": function (data, type, row) {
            return `<a class="btn btn-sm btn-info" target="_blank" href="{{ asset('img/member/receipt/`+data.payment_receipt+`') }}">Lihat Gambar</a>`
          }
        },
        {
          "data": null,
          "render" : function (data, type, row) {
              if (data.payment_status == 'pending') {
                return '<button class="btn btn-sm btn-success change-status" data-toggle="tooltip" data-id="'+data.id+'" title="Klik untuk konfirmasi pembayaran">Konfirmasi</button>';
              } else {
                return '<button disabled class="btn btn-sm btn-secondary">Konfirmasi</button>';
              }
          }
        },
      ],
    });

    // increment number
    table.on( 'draw.dt', function () {
      let increment = $('#transactionTable').DataTable().page.info();
          table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + increment.start;
        });
    });

    // filter month
    $('.filterMonth').click(function() {
      let date = new Date();
      let year = date.getFullYear();
      let month = $(this).data('month');

      table.search(month + ' ' + year).draw();
    });
    
    // reset the filter
    $('#reset').click(function() {
      $('.filterMonth').removeClass('active');
      table.search('').draw();
    });

    // change status
    $('#transactionTable tbody').on( 'click', '.change-status', function () {
      let status = $( "a.status-tab[aria-selected='true']" ).data('status');
      let id = $(this).data('id');

      confirmPayment(id, status);
    });

    // filter status
    $('.status-tab').click(function() {
      let status = $(this).data('status');
      table.ajax.url("/admin/transactions-data?status="+status).load();
    });

  });
</script>
@endsection
