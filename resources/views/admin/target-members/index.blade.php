@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Target Pelanggan</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Target Pelanggan</div>
      </div>
    </div>

    <div class="section-body">

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Data Target Pelanggan</h4>
            </div>
            <div class="card-body">
              <div>
                {{-- <a href="{{ route('target-members.create') }}" class="btn btn-sm btn-primary mr-1 mb-3"><i
                    class="fas fa-plus"></i> Tambah Data</a> --}}
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-bordered" id="hamletTable">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No.
                      </th>
                      <th>Nama Dusun</th>
                      <th>Target Pelanggan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($targetMembers as $target)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $target->hamlet->hamlet_name }}</td>
                      <td>{{ $target->target_qty }}</td>
                      <td>
                        <a href="{{ route('target-members.edit', $target->id) }}" class="btn btn-sm btn-warning mr-1"
                        data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
@if (Session::has('success'))
<script>
iziToast.success({
  position: 'topRight',
  title: 'Sukses',
  message: '{{ Session::get("success") }}',
  timeout: 3000
});
</script>
@endif
<script>
$(document).ready(function() {
  $('#hamletTable').DataTable();
});
</script>
@endsection
