@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Form Validation</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Kelola Data</a></div>
        <div class="breadcrumb-item"><a href="#">Dusun</a></div>
        <div class="breadcrumb-item">Tambah Data</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <form method="post" action="{{ route('target-members.update', $targetMember) }}" class="needs-validation" novalidate="">
                {{ method_field('PUT') }}
                @csrf
              <div class="card-header">
                <h4>Tambah Data</h4>
              </div>
              <div class="card-body">

                <div class="form-group">
                    <label>Dusun</label>
                    <input type="hidden" name="hamlet_id" value="{{$targetMember->id}}">
                    <input type="text" class="form-control" value="{{$targetMember->hamlet->hamlet_name}}" readonly>
                    <div class="invalid-feedback">
                    Isian masih kosong
                    </div>
                </div>

                <div class="form-group">
                  <label>Jumlah Target</label>
                  <input type="number" class="form-control" name="target_qty" required="" value="{{$targetMember->target_qty}}">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>

                <div class="card-footer text-right">
                  <button class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
