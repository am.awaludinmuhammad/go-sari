@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Detail Pelanggan</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="">Member</a></div>
        <div class="breadcrumb-item">Detail</div>
      </div>
    </div>
    <div class="section-body">
      <div class="row">
      <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4>Detail Profil</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-3">
                    @if($user->profile_picture == "avatar-default.png")
                    <img src="{{ asset('img/avatar-default.png') }}" id="profileImg" class="img-fluid element1 profile-pic" alt="" style="min-height:100px; min-width:100px" data-img="default" />
                    @else
                    <img src="{{ asset('img/member/avatar/'.$user->profile_picture) }}" id="profileImg" class="img-fluid element1 profile-pic" alt="" data-img="user" />
                    @endif  
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label>Nama Lengkap</label>
                      <input type="text"  readonly class="form-control" name="name" value="{{ $user->name }}" >
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input  readonly type="email" class="form-control" name="email" value="{{ $user->email }}" >
                    </div>

                    <div class="form-group">
                      <label>NIK</label>
                      <input  readonly type="email" class="form-control" name="email" value="{{ $user->nik }}" >
                    </div>

                    <div class="form-group">
                      <label>No HP</label>
                      <input readonly type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                    </div>

                    <div class="form-group">
                      <label>Kategori Sampah</label>
                      <input readonly type="text" class="form-control" name="phone" value="{{ $user->garbageCategory->category_name }}">
                    </div>

                    <div class="form-group">
                      <label>Letak Sampah</label>
                      <input readonly type="text" class="form-control" name="phone" value="{{ $user->garbage_can_location }}">
                      <div class="mt-2">
                        <a href="{{ asset('img/member/garbage'). '/' . $user->garbage_can_picture }}" target="_blank">Lihat Foto</a>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="hamlet" class="form-label">Dusun</label>
                      <input  readonly class="form-control" id="hamlet" name="hamlet_id" value="{{ $hamlet->hamlet_name }}">
                    </div>

                    <div class="form-group">
                      <label for="rt" class="form-label">RT</label>
                      <input  readonly class="form-control" id="rt" name="rt" value="{{ $user->rt }}">
                    </div>

                    <div class="form-group">
                      <label for="address" class="form-label">Alamat Lengkap</label>
                      <textarea name="address" readonly class="form-control @error('address') is-invalid @enderror"
                        id="address" rows="2" style="height: 100px">{{ $user->address }}</textarea>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
       
      </div>
      <div class="row mt-2 mt-sm-2">
        <div class="col-md-8 col-sm-12">
          
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
