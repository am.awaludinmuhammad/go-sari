@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>{{$menu}}</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ $url }}">{{$menu}}</a></div>
        <div class="breadcrumb-item">{{ $page }}</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <form method="post" action="{{ route('setting.banks.update', $bank->id) }}" class="needs-validation" novalidate="">
            @csrf
              <div class="card-header">
                <h4>{{ $page }}</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Bank</label>
                  <input type="text" class="form-control" name="name" required="" value="{{ $bank->name }}">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Nomor Rekening</label>
                  <input type="text" class="form-control" name="number" required="" value="{{ $bank->number }}">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Atas Nama</label>
                  <input type="text" class="form-control" name="owner" required="" value="{{ $bank->owner }}">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" name="status" required="">
                    <option value="1" {{ $bank->status == '1'? 'selected': '' }}>Aktif</option>
                    <option value="0" {{ $bank->status == '0'? 'selected': '' }}>Tidak Aktif</option>
                  </select>
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
