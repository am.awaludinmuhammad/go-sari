@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>{{$menu}}</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">{{ $page }}</div>
      </div>
    </div>

    <div class="section-body">

    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>{{ $menu }}</h4>
            </div>
            <div class="card-body">
            <div>
                <a href="{{ route('setting.banks.create') }}" class="btn btn-sm btn-primary mr-1 mb-3"><i
                    class="fas fa-plus"></i> {{ $page }}</a>
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-bordered" id="bankTable">
                  <thead>
                    <tr>
                      <th class="text-center">No.</th>
                      <th class="text-center">Nama Bank</th>
                      <th class="text-center">Nomor Rekening</th>
                      <th class="text-center">Atas Nama</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($banks as $bank)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $bank->name }}</td>
                      <td>{{ $bank->number }}</td>
                      <td>{{ $bank->owner }}</td>
                      @if($bank->status == '1')
                        <td>Aktif</td>
                      @else
                      <td>Tidak Aktif</td>
                      @endif
                      <td>
                        <a href="{{ route('setting.banks.edit', $bank->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i></a>
                        <form action="{{ route('setting.banks.destroy', $bank->id) }}" method="post"  style="display: inline-block;">@csrf
                          <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    $('#bankTable').dataTable();
  });
</script>
@endsection
