@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Kategori Sampah</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ url('admin/garbage-categories') }}">Kategori Sampah</a></div>
        <div class="breadcrumb-item">Tambah Kategori</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <form method="post" action="{{ route('garbage-categories.store') }}" class="needs-validation" novalidate="">
              @csrf
              <div class="card-header">
                <h4>Tambah Kategori</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Kategori</label>
                  <input type="text" class="form-control" name="category_name" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Harga</label>
                  <input type="number" class="form-control" name="price" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
