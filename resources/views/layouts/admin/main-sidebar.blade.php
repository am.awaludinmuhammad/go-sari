<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ url('/') }}">Go Sari</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ url('/') }}">GS</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Dashboard</li>

      <li class="nav-item dropdown {{ request()->is('admin/dashboard') ? 'active' : '' }}">
        <a href="{{ url('admin/dashboard') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      </li>

      <li class="menu-header">Menu</li>

      <li class="nav-item dropdown {{ request()->is('admin/garbage-categories') || request()->is('admin/garbage-categories/*') || request()->is('admin/hamlets') || request()->is('admin/hamlets/*') || request()->is('admin/target-members/') || request()->is('admin/target-members/*') ? 'active' : '' }}">
        <a href=" #" class="nav-link has-dropdown"><i class="fas fa-database"></i> <span>Manajemen</span></a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('hamlets.index') }}">Dusun</a></li>
          <li><a href="{{ route('garbage-categories.index') }}">Kategori Sampah</a></li>
          <li><a href="{{ route('target-members.index')}}">Target Pelanggan</a></li>
          <li><a href="{{ route('setting.banks.index') }}">Akun Bank</a></li>
        </ul>
      </li>

      <li><a href="{{ route('members.index') }}"><i class="fas fa-user"></i> <span>Pelanggan</span></a></li>

      <li><a id="transactionsLink" href="{{ route('invoices.index') }}"><i class="fas fa-money-bill" id="transaction-nav-link"></i><span>Tagihan</span></a></li>

      <li><a id="transactionsLink" href="{{ route('transactions.index') }}"><i class="fas fa-money-check" id="transaction-nav-link"></i><span>Transaksi Pembayaran</span></a></li>

      <li><a id="transactionsLink" href="{{ route('report.index') }}"><i class="fas fa-chart-bar"></i> <span>Laporan Transaksi</span></a></li>
      
      @if(Auth::guard('admin')->user()->role == "superadmin")
      <li><a class="nav-link" href="{{ url('super-admin/admins') }}"><i class="fas fa-users-cog"></i> <span>Kelola Admin</span></a></li>
      @endif
    </ul>
  </aside>
</div>
