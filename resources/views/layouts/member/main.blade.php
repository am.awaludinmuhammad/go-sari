<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Go Sari - Unit Pelayanan Kebersihan Lingkungan</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/landing-page/main.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/member/dashboard/main.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/member/dashboard/calendar.css') }}" />
      <link rel="shortcut icon" href="{{ asset('img/favicon-icon.png') }}">
      
      <link rel="stylesheet" type="text/css"href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

      <style>
          @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;800&display=swap");
        body {
          margin: 0px !important;
          padding: 0px !important;
          overflow-x: hidden;
        }

        * {
          font-family: 'Poppins', sans-serif;
          margin: 0;
          padding: 0;
          -webkit-box-sizing: border-box;
                  box-sizing: border-box;
        }

        .shrink .custom-navbar {
          z-index: 6;
          -webkit-box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
                  box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
          background: #fff;
          -webkit-animation: fadeInDown;
                  animation: fadeInDown;
          /* referring directly to the animation's @keyframe declaration */
          -webkit-animation-duration: 1s;
                  animation-duration: 1s;
        }

        .section-line {
          width: 60px;
          height: 2px;
          background: #05A102;
          margin: 30px auto;
          display: block;
        }

        .custom-navbar {
          z-index: 5;
          -webkit-box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
                  box-shadow: 0px 10px 30px rgba(218, 218, 218, 0.671);
          background: #fff;
        }

        .custom-navbar .navbar-brand .element3 {
          position: relative;
          z-index: 6;
          width: 200px;
          margin-top: -100px !important;
        }
      </style>
      <!-- My style -->
      <link rel="stylesheet" href="{{ asset('css/landing-page/landing-style.css') }}">

      @yield('style')
      <title>Document</title>
  </head>
  <body>
    <section class="ladingPage">
      <!-- Navbar -->
      @include('layouts.member.navbar')
      
    </section>
  <!-- Content -->
    @yield('content')

  <!-- Footer -->
  @include('layouts.member.footer')
  
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <script src="https://kit.fontawesome.com/abae7fb329.js" crossorigin="anonymous"></script>

    <script src="{{ asset('js/admin/stisla.js') }}"></script>
    <script src="{{ asset('js/admin/scripts.js') }}"></script>
  @yield('script')
  </body>
</html>