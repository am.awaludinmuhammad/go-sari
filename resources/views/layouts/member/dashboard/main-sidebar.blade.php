<?php
$user = Auth::user();
?>
<div class="main-sidebar">
  <aside id="sidebar-wrapper">
  <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ url('/') }}">GS</a>
    </div>
    <div class="sidebar-brand">
      <a href="#">Go <span>Sari</a></span>
      <div class="img-profil">
        @if($user->profile_picture == "avatar-default.png")
        <img src="{{ asset('img/avatar-default.png') }}" class="img-fluid element1" alt="" />
        @else
        <img src="{{ asset('img/member/avatar/'.$user->profile_picture) }}" id="sidebarAvatar" class="img-fluid element1" alt="" />
        @endif
      </div>
      <div class="greeting">
        <div class="welcome">
          <p>Welcome</p>
        </div>
        <div class="name">
          <p id="nama">{{ $user->name }}</p>
        </div>
      </div>
    </div>
     
    <ul class="sidebar-menu">
      
      <li class="nav-item {{ request()->is('dashboard') ? 'active' : '' }}">
        <a href="{{ url('/dashboard') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      </li>
      <li class="nav-item dropdown {{ request()->is('transactions') ? 'active' : '' }}">
        <a href="{{ url('transactions') }}" class="nav-link"><i class="fas fa-wallet"></i><span>Transaksi</span></a>
      </li>

      <li class="nav-item dropdown {{ request()->is('profile') ? 'active' : '' }}">
        <a href="{{ url('profile') }}" class="nav-link"><i class="fas fa-user"></i><span>Profil</span></a>
      </li>

      {{-- <li class="nav-item sign-out">
        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i><span class="text-danger">Keluar</span></a>
      </li>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form> --}}
    </ul>
  </aside>
</div>