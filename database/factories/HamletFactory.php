<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Hamlet;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Hamlet::class, function (Faker $faker) {
    return [
        'hamlet_name' => $faker->name
    ];
});
