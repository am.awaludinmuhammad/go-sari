<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Models\Hamlet;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\GarbageCategory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $num = '012345678912';
    $hamlets = Hamlet::pluck('id')->toArray();
    $categories = GarbageCategory::pluck('id')->toArray();

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'nik' => str_shuffle($num).str_shuffle('1234'),
        'phone' => str_shuffle($num),
        'garbage_category_id' => $categories[array_rand($categories)],
        'garbage_can_location' => 'Belakang',
        'garbage_can_picture' => 'GarbageImg_20210731_164911.png',
        'hamlet_id' => $hamlets[array_rand($hamlets)],
        'rt' => '10',
        'address' => $faker->address,
        'profile_picture' => 'avatar-default.png',
        'status' => User::ACTIVE,
        'remember_token' => Str::random(10),
    ];
});
