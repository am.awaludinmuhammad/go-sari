<?php

use App\Models\TargetMember;
use Illuminate\Database\Seeder;

class TargetMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = [
            'hamlet_id' => '1',
            'target_qty' => 50,
            'created_at' => now(),
            'updated_at' => now()
        ];
        TargetMember::insert($record);
    }
}
