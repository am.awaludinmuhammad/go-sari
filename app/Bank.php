<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['name', 'number', 'owner', 'status'];
    const ACTIVE = 1;
    const INACTIVE = 0;
}
