<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = Auth::guard('admin')->user();
        if ($admin->role == "superadmin") {
            return $next($request);
        } else {
            return redirect('/admin/dashboard')->with('error', 'Anda tidak memiliki hak akses.');
        }
        

    }
}
