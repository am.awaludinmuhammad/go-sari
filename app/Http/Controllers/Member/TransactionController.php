<?php

namespace App\Http\Controllers\Member;
use App\User;
use App\Models\Invoice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
 public function index() {
    $user = Auth::user();
    $transactions = Invoice::where('user_id', $user->id)
        ->where('payment_status', '!=', Invoice::UNPAID)
        ->latest()
        ->take(4)
        ->get();

    $invoiceMonth = Controller::formatMonthIndo(date('m')) . ' ' . date('Y');

    $currentInvoice = Invoice::where('user_id', $user->id)
        ->where('month', $invoiceMonth)
        ->first();
    
    return view('member.transactions', compact('user', 'transactions', 'invoiceMonth', 'currentInvoice'));
 }
}
