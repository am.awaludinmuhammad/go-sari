<?php

namespace App\Http\Controllers\Member;

use App\Models\Hamlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //
    public function index()
    {
      $user = Auth::user();
      $hamlets = Hamlet::all();

      return view('member.profile', compact('user', 'hamlets'));
    }
    public function editProfile(Request $request)
    {
        return view('member.edit-profile', [
            'user' => $request->user()
        ]);
    }

    public function updateProfile(Request $request)
    {
        $request->user()->update(
            $request->all()
        );

        return redirect('profile')->with('success', 'Profil berhasil diupdate.');
    }
    public function saveAvatar(Request $request)
    {
        request()->validate([
            'profile_picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $user = Auth::user();
        if ($files = $request->file('profile_picture')) {
            
            $img = request()->file('profile_picture');
            $imgName = "avatar_" . $user->name . '.'.$img->getClientOriginalExtension();
            $img->move('img/member/avatar',$imgName);
            
            $user->profile_picture = $imgName;
            $user->save();

            return response()->json([
                'success' => true,
                'message'=> 'Foto profil berhasil diubah.'
            ], 200);
 
        }
 
    }
}
