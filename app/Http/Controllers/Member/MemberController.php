<?php

namespace App\Http\Controllers\Member;

use PDF;
use App\Bank;
use App\User;
use Notification;
use Carbon\Carbon;
use App\Models\Admin;
use App\Models\Hamlet;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewTransactionNotification;

class MemberController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user();
        $transactions = Invoice::where('user_id', $user->id)
            ->where('payment_status', '!=', Invoice::UNPAID)
            ->latest()
            ->take(4)
            ->get();

        $invoiceMonth = Controller::formatMonthIndo(date('m')) . ' ' . date('Y');

        $currentInvoice = Invoice::where('user_id', $user->id)
            ->where('month', $invoiceMonth)
            ->first();
        
        return view('member.dashboard', compact('user', 'transactions', 'invoiceMonth', 'currentInvoice'));
    }

    public function getInvoices()
    {
        $invoices = Invoice::where('user_id', Auth::user()->id)->get();
        return view('member.invoices', compact('invoices'));
    }

    public function getInvoiceDetail($code)
    {
        $invoice = Invoice::where('code', $code)->first();
        return view('member.invoice-detail', compact('invoice'));
        
    }

    public function getInvoiceGenerate($code)
    {
        $invoice = Invoice::where('code', $code)->first();
        $data = [
            'invoice' => $invoice,
            'description' => 'This description of Funda of Web IT'
        ];
        $pdf = PDF::loadView('member.invoice-order', $data);
        // $pdf->stream("dompdf_out.pdf", array("Attachment" => false));

        // exit(0);
        return $pdf->download($code.'.pdf');

        // return view('member.invoice-order', compact('invoice'));
    }
    

    public function showPaymentPage($code)
    {
        $invoice = Invoice::where('code', $code)->first();
        $banks = Bank::where('status', Bank::ACTIVE)->take(4)->get();

        return view('member.payment', compact('invoice', 'banks'));
    }

    public function formUploadPayment($code)
    {   

        return view('member.upload-receipt', compact('code'));
    }

    public function uploadPaymentReceipt(Request $request)
    {
        $code = $request->code;
        $userId = Auth::user()->id;
        $img = request()->file('payment_receipt');
        $imgName = "Receipt_" . $userId . '_' . date('Ym') .'.'.$img->getClientOriginalExtension();

        $img->move('img/member/receipt',$imgName);

        $invoice = Invoice::where('code', $code)->first();
        $invoice->payment_status = Invoice::PENDING;
        $invoice->payment_receipt = $imgName;
        $invoice->payment_date = Carbon::now();
        $invoice->save();

        // push notification
        $admins = Admin::all();
        $user = User::where('id', $userId)->first();
        Notification::send($admins, new NewTransactionNotification($user));

        return redirect('dashboard')->with('success', 'Upload berhasil');        
    }

    public function markNotification(Request $request)
    {
        $user = Auth::user();

        $user->unreadNotifications
            ->when($request->input('id'), function ($query) use ($request) {
                return $query->where('id', $request->input('id'));
            })
        ->markAsRead();
           
        return response()->json(['message' => 'Notifikasi telah dibaca.']);
    }
}
