<?php

namespace App\Http\Controllers\Admin;

use App\Models\Hamlet;
use App\Http\Controllers\Controller;
use App\Models\TargetMember;
use Illuminate\Http\Request;

class TargetMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $targetMembers = TargetMember::all();

        return view('admin.target-members.index', compact('targetMembers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hamlets = Hamlet::all();

        return view('admin.target-members.create', compact('hamlets'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'hamlet_id' => 'required',
            'target_qty' => 'required'
        ]);

        TargetMember::updateOrCreate(
        ['hamlet_id' => $request->hamlet_id],
        ['target_qty' => $request->target_qty]);

        return redirect()->route('target-members.index')->with('success', 'Data berhasil ditambahkan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $targetMember = TargetMember::find($id);

        return view('admin.target-members.edit', compact('targetMember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'target_qty' => 'required',
        ]);

        $targetMember = TargetMember::find($id);
        $targetMember->target_qty = $request->target_qty;
        $targetMember->save();

        return redirect()->route('target-members.index')->with('success', 'Data berhasil disimpan.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $targetMember = TargetMember::find($id);
        $targetMember->delete();
        return redirect()->route('target-members.index')->with('success', 'Data berhasil dihapus.');
    }
}
