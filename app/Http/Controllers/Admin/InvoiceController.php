<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\Payment;
use PDF;
use Illuminate\Http\Request;
use App\Models\TransactionSuccess;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SettingController;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $months = Controller::getIndoMonth();
        
        return view('admin.invoices.index', compact('months'));
    }

    public function getData(Request $request)
    {
        $status = $request->status;
        if ($status == "all") {
            $data = Invoice::join('users', 'users.id', '=', 'invoices.user_id')
            ->get([
                'invoices.id',
                'month',
                'code',
                'name',
                'total_amount',
                'payment_due',
                'payment_status',
            ]);
        }

        if ($status == "unpaid") {
            $data = Invoice::where('payment_status', '!=', Invoice::PAID)
            ->join('users', 'users.id', '=', 'invoices.user_id')
            ->get([
                'invoices.id',
                'month',
                'code',
                'name',
                'total_amount',
                'payment_due',
                'payment_status',
            ]);
        }
        
        if($status == "paid") {
            $data = Invoice::where('payment_status', Invoice::PAID)
            ->join('users', 'users.id', '=', 'invoices.user_id')    
            ->get([
                'invoices.id',
                'month',
                'code',
                'name',
                'total_amount',
                'payment_due',
                'payment_status',
            ]);
        }
            
        return response()->json([
            'data' => $data
        ]);
    }

    public function count()
    {
      $all = Invoice::count();
      $paid = Invoice::where('payment_status', Invoice::PAID)->count();
      $unpaid = Invoice::where('payment_status', '!=', Invoice::PAID)->count();

      return response()->json([
          'all' => $all,
          'paid' => $paid,
          'unpaid' => $unpaid,
      ],200);
    }

    public function create()
    {
        $months = Controller::getIndoMonth();
        $pastYear = Carbon::now()->subYears(2)->format('Y');
        $nextYear = Carbon::now()->addYears(4)->format('Y');
        $years = range($pastYear, $nextYear);
        
        return view('admin.invoices.create', compact('months', 'years'));
    }

    public function store(Request $request)
    {
        $members = $request->members;
        $invoiceMonth = $request->month . " " . $request->year;
        $dueDate = $request->payment_due;

        $members = User::where('status', User::ACTIVE)->get();
        foreach($members as $member){
            $targetInvoice = Invoice::where('user_id', $member->id)
                ->where('month', $invoiceMonth)->first();

            if (empty($targetInvoice)) {
                Invoice::create([
                    'user_id' => $member->id,
                    'month' => $invoiceMonth,
                    'code' => Invoice::generateCode($dueDate),
                    'payment_due' => $dueDate,
                    'payment_status' => Invoice::UNPAID,
                    'total_amount' => $member->garbageCategory->price
                ]);
            }
        }

        return redirect('admin/invoices')->with('success', 'Data berhasil ditambahkan.');
    }

    public function show($id)
    {
        $invoice = Invoice::where('code', $id)->first();

        return view('admin.invoices.details', compact('invoice'));
    }

    public function downloadInvoice($code)
    {
        $invoice = Invoice::where('code', $code)->first();
        $data = [
            'invoice' => $invoice,
            'description' => 'This description of Funda of Web IT'
        ];
        $pdf = PDF::loadView('member.invoice-order', $data);
        return $pdf->download($code.'.pdf');

    }

    public function confirmPaymentManual(Request $request)
    {
        $invoice = Invoice::find($request->id);
        $invoice->payment_status = Invoice::PAID;
        $invoice->payment_date = Carbon::now();
        $invoice->save();

        return response()->json([
            'success' => true,
            'message' => 'Status pembayaran berhasil dirubah.',
        ], 200);

    }

    public function destroy($id)
    {
        Invoice::find($id)->delete();
        
        return response()->json([
            "success" => true,
            "message" => "Data berhasil dihapus."
        ], 200);
    }
}
