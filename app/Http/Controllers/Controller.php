<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    static function getIndoMonth()
    {
        $data = array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return ($data);
    }

    public static function formatMonthIndo($no)
    {
        $bln["01"] = "Januari";
        $bln["02"] = "Februari";
        $bln["03"] = "Maret";
        $bln["04"] = "April";
        $bln["05"] = "Mei";
        $bln["06"] = "Juni";
        $bln["07"] = "Juli";
        $bln["08"] = "Agustus";
        $bln["09"] = "September";
        $bln["10"] = "Oktober";
        $bln["11"] = "November";
        $bln["12"] = "Desember";

        return $bln[$no];
    }
}
