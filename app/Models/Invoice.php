<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'user_id',
        'code',
        'month',
        'payment_due',
        'payment_date',
        'total_amount',
        'payment_status',
        'payment_receipt',
    ];

    const UNPAID = 'unpaid';
    const PENDING = 'pending';
    const PAID = 'paid';

    /**
     * Get the user that owns the invoice.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function generateCode($dueDate)
    {       
        $code = 'INV-' . date("Ym",strtotime($dueDate)) . '-';
        
        $lastInvoice = self::select([DB::raw('MAX(invoices.code) AS last_code')])
            ->where('code', 'like', $code . '%')
            ->first();       
        
        $lastInvoiceCode = !empty($lastInvoice) ? $lastInvoice['last_code'] : null;
        
        $invoiceCode = $code . '0001';
        if ($invoiceCode) {
            $lastInvoiceNumber = str_replace($code, '', $lastInvoiceCode);
            $nextInvoiceNumber = sprintf('%04d', (int)$lastInvoiceNumber + 1);
            
            $invoiceCode = $code . $nextInvoiceNumber;
        }
        if (self::_isInvoiceCodeExist($invoiceCode)) {
            return generateCode($dueDate);
        }

        return $invoiceCode;
    }

    private static function _isInvoiceCodeExist($invoiceCode)
    {
        return self::where('code', '=', $invoiceCode)->exists();
    }

}
