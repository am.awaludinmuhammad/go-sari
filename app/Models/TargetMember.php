<?php

namespace App\Models;

use App\Models\Hamlet;
use Illuminate\Database\Eloquent\Model;

class TargetMember extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hamlet_id',
        'target_qty'
    ];


    /**
     * Get the user that owns the invoice.
     */
    public function hamlet()
    {
        return $this->belongsTo(Hamlet::class);
    }
}
