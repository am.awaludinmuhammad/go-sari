<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Hamlet;

class HamletTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use DatabaseMigrations;
    use WithFaker;


    public function testIndex()
    {
        $this->withoutMiddleware();
        $admin = factory('App\Models\Admin')->create();
        $this->actingAs($admin, 'admin');

        $hamlet = factory('App\Models\Hamlet')->create();
        $response = $this->get('admin/hamlets')->assertStatus(200);
        $response->assertSee($hamlet->name);
    }

    public function testCreateHamlet()
    {
        $this->withoutMiddleware();
        $admin = factory('App\Models\Admin')->create();
        $this->actingAs($admin, 'admin');

        $data = collect(
            [
                'hamlet_name' => 'rama'
            ]
        );

        $this->post('admin/hamlets', $data->toArray())
            ->assertStatus(302);
        $this->assertEquals(1, Hamlet::all()->count());
    }

    public function testShowHamlet()
    {
        $admin = factory('App\Models\Admin')->create();
        $this->actingAs($admin, 'admin');

        $hamlet = factory('App\Models\Hamlet')->create();
        $response = $this->get('admin/hamlets' . $hamlet->id);
        $response->assertSee($hamlet->name);
    }

    public function testUpdateHamlet()
    {
        $admin = factory('App\Models\Admin')->create();
        $this->actingAs($admin, 'admin');

        $hamlet = factory('App\Models\Hamlet')->create([
            'hamlet_name' => 'Rama'
        ]);
        $hamlet->hamlet_name = "update m";


        $this->put('/admin/hamlets/' . $hamlet->id, $hamlet->toArray());
        $this->assertDatabaseHas('hamlets', ['id' => $hamlet->id, 'hamlet_name' => 'update m']);
    }

    public function testDelete()
    {
        $admin = factory('App\Models\Admin')->create();
        $this->actingAs($admin, 'admin');

        $hamlet = factory('App\Models\Hamlet')->create([
            'hamlet_name' => 'Rama'
        ]);

        $this->delete('/admin/hamlets/' . $hamlet->id);
        $this->assertDatabaseMissing('hamlets', ['id' => $hamlet->id]);
    }
}